 ________________________________________________
/ This repository has been moved to codeberg.    \
\ https://codeberg.org/ManfredLotz/fish_globabbr /
 ------------------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

# fish_globabbr

The project offers an implementation of the `abbr` functionality in the fish shell with the additional benefit that abbreviations get expanded not only at the beginning of the command line.


# Background

Want to have global aliases in a way the zsh shell offers it. One important
feature of the global alias in the zsh shell is that it gets expanded anywhere
in the command line, i.e. not only at the beginning

The beauty of the zsh implementation is that the SPACE key could be used to expand
the global alias.

Global alias is dealt with here: https://github.com/fish-shell/fish-shell/issues/5003

## fish has `abbr` but expansions happen only at the beginning of the command line

The fish shell has the `abbr` functionality which expands only at the beginning of the
command line.

As for the `abbr` functionality the SPACE key is already used it cannot be bound to anything else. 
If it would bind SPACE to my own global alias facility then the `abbr` functionality is no longer available.

So if I want to keep the SPACE key for my global alias and using the fish `abbr` functionality as well
something new has to be written covering both functionalities. 

# Current implementation

The code is actually taken from https://github.com/fish-shell/fish-shell/issues/731 and slightly modified.
I am aware that my implementation doesn't fully cover the fish `abbr` functionality but for my purposes
it seems to work fine.



# Installation

Put `globabbr.fish` into `~/.config/fish/conf.d/globabbr.fish`

# How to use it?


## Assume you don't want to have the `vlc` command in the history

```
  globabbr vlc ' vlc'
```

## You want just type `G<space> `and get `| grep` anywhere then define

```
  globabbr  G   '| grep'
```

Now you easily may type:   `ls -l G<space>` and get `ls -l | grep` 

